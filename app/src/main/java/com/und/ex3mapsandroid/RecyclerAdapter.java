package com.und.ex3mapsandroid;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.und.ex3mapsandroid.databinding.RestaurantItemBinding;
import com.und.ex3mapsandroid.helper.ItemTouchHelperAdapter;
import com.und.ex3mapsandroid.model.Restaurant;
import com.und.ex3mapsandroid.utilities.OnRestListChangedListener;
import com.und.ex3mapsandroid.utilities.OnStartDragListener;

import java.util.Collections;
import java.util.List;




public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerViewHolder> implements ItemTouchHelperAdapter {

    private List<Restaurant> restaurantList;
    private Context context;
    private OnStartDragListener startDragListener;
    private OnRestListChangedListener restListChangedListner;

    public RecyclerAdapter(List<Restaurant> restaurantList, Context context, OnStartDragListener dragListener, OnRestListChangedListener restListChangedListner) {
        this.context = context;
        this.restaurantList = restaurantList;
        this.startDragListener = dragListener;
        this.restListChangedListner = restListChangedListner;
    }

    public void setRestaurantList(List<Restaurant> restaurantList) {
        this.restaurantList = restaurantList;
        notifyDataSetChanged();
    }

    public void updateList(List<Restaurant> restaurantList) {
        this.restaurantList = restaurantList;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RestaurantItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.single_row, parent, false);
        return new RecyclerViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {
        holder.getBinding().setRestaurant(restaurantList.get(position));
//        holder.getBinding().getRoot().findViewById(R.id.image).setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                if (MotionEventCompat.getActionMasked(event) ==
//                        MotionEvent.ACTION_DOWN) {
//                    startDragListener.onStartDrag(holder);
//                }
//                return false;
//            }
//        });


    }

    @Override
    public int getItemCount() {
        return restaurantList.size();
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(restaurantList, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(restaurantList, i, i - 1);
            }
        }
        restListChangedListner.onRestListOrderChanged(restaurantList);
        notifyItemMoved(fromPosition, toPosition);
        return true;

    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {


        ImageView handleView;
        private RestaurantItemBinding binding;

        public RecyclerViewHolder(final RestaurantItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.executePendingBindings();
            this.binding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, LocationDetails.class);
                    intent.putExtra(Restaurant.EXTRA_KEY, restaurantList.get(getAdapterPosition()));
                    context.startActivity(intent);
                }
            });


        }

        public RestaurantItemBinding getBinding() {
            return binding;
        }
    }
}

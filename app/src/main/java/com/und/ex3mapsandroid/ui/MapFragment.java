package com.und.ex3mapsandroid.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.DirectionsApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.PendingResult;
import com.google.maps.internal.PolylineEncoding;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsRoute;
import com.und.ex3mapsandroid.LocationDetails;
import com.und.ex3mapsandroid.R;
import com.und.ex3mapsandroid.base.BaseFragment;
import com.und.ex3mapsandroid.model.Coordinate;
import com.und.ex3mapsandroid.model.PolyLineData;
import com.und.ex3mapsandroid.model.Restaurant;
import com.firebase.client.Firebase;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

/**
 * Plot {@link Coordinate} into map
 */
public class MapFragment extends BaseFragment implements OnMapReadyCallback, OnInfoWindowClickListener, GoogleMap.OnPolylineClickListener {

    private static final String TAG = "MapFragment";

    private List<Restaurant> restaurantList;
    private GoogleMap googleMap;
    private HashMap<String, RestaurantLocationMarkerMetadata> restaurantMap;
    private GoogleApiClient client;
    private LatLng latLng;
    Switch switchCompact;
    // only use one shared preference
    private SharedPreferences sharedPreferences;
    private Marker userLocation;
    private GeoApiContext geoApiContext = null;
    private Marker selectedMarker;
    private ArrayList<PolyLineData> mPolyLinesData = new ArrayList<>();


    public static MapFragment newInstance(List<Restaurant> restaurantList, LatLng latLng) {
        Log.d(TAG, "newInstance() called with: restaurantList = [" + restaurantList + "], latLng = [" + latLng + "]");
        MapFragment mapFragment = new MapFragment();
        mapFragment.restaurantList = new ArrayList<>(restaurantList);
        mapFragment.restaurantMap = new HashMap<>();
        mapFragment.latLng = latLng;
        return mapFragment;
    }

    @Override
    public int layout() {
        return R.layout.fragment_map;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // initialize map
        SupportMapFragment fragment = new SupportMapFragment();
        getChildFragmentManager().beginTransaction().add(R.id.map_container, fragment).commit();
        fragment.getMapAsync(this);
        if(geoApiContext==null){
            geoApiContext = new GeoApiContext.Builder().apiKey(getString(R.string.google_api_key)).build();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(LayoutInflater.from(getContext())));
        googleMap.setOnInfoWindowClickListener(this);
        setupClustering(createMetadataList(restaurantList)  );
        if (latLng != null) onLocationUpdate(latLng);
        googleMap.setOnPolylineClickListener(this);


    }
    private void calculateDirections(Marker marker){
        Log.d(TAG, "calculateDirections: calculating directions.");

        com.google.maps.model.LatLng destination = new com.google.maps.model.LatLng(
                marker.getPosition().latitude,
                marker.getPosition().longitude
        );
        DirectionsApiRequest directions = new DirectionsApiRequest(geoApiContext);

        directions.alternatives(true);
        directions.origin(
                new com.google.maps.model.LatLng(
                        47.923304,  -97.086233
                )
        );
        Log.d(TAG, "calculateDirections: destination: " + destination.toString());
        directions.destination(destination).setCallback(new PendingResult.Callback<DirectionsResult>() {
            @Override
            public void onResult(DirectionsResult result) {
                addPolylinesToMap(result);
                Log.d(TAG, "calculateDirections: routes: " + result.routes[0].toString());
                Log.d(TAG, "calculateDirections: duration: " + result.routes[0].legs[0].duration);
                Log.d(TAG, "calculateDirections: distance: " + result.routes[0].legs[0].distance);
                Log.d(TAG, "calculateDirections: geocodedWayPoints: " + result.geocodedWaypoints[0].toString());
            }

            @Override
            public void onFailure(Throwable e) {
                Log.e(TAG, "calculateDirections: Failed to get directions: " + e.getMessage() );

            }
        });
    }
    @Override
    public void onLocationUpdate(LatLng latLng) {
        System.out.println(latLng);
        if (userLocation != null) userLocation.remove();
        MarkerOptions options = new MarkerOptions();
        options.position(latLng);
        options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        options.title("Your location");
        userLocation = googleMap.addMarker(options);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14));
    }

    private List<RestaurantLocationMarkerMetadata> createMetadataList(List<Restaurant> restaurantList) {
        Log.d(TAG, "createMetadataList() called with: restaurantList = [" + restaurantList + "]");
        // initial check
        List<RestaurantLocationMarkerMetadata> metadataList = new ArrayList<>();
        // remove markers if there
        for (final Restaurant restaurant : restaurantList) {
            if (restaurant.getCoordinateList() == null && restaurant.getCoordinateList().isEmpty()) continue;
            for (Coordinate coordinate : restaurant.getCoordinateList()) {
              if (coordinate.getLatitude() == Double.MIN_NORMAL) continue;
                metadataList.add(new RestaurantLocationMarkerMetadata(restaurant, coordinate));
            }
        }
        return metadataList;
    }

    @Override
    public void onInfoWindowClick(final Marker marker) {
        Toast.makeText(getActivity(), "Info window clicked",
                Toast.LENGTH_SHORT).show();
        final RestaurantLocationMarkerMetadata metadata = restaurantMap.get(marker.getId());
        // start dialog here
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Determine the route");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // 1) view restaurant details
                if (restaurantMap.get(marker.getId()) != null) {
                    selectedMarker = marker;
                    calculateDirections(marker);



                }
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // 2) change coordinate status
                dialogInterface.dismiss();

            }
        });

        // 3. Get the AlertDialog from create()
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public Fragment getFragment() {
        return this;
    }

    @Override
    public void refreshData(List<Restaurant> restaurantList) {
        // Remove all markers and plot new markers
        setupClustering(createMetadataList(new ArrayList<Restaurant>(restaurantList)));
    }

    @Override
    public void onPolylineClick(Polyline polyline) {
        for(PolyLineData polylineData: mPolyLinesData){
            Log.d(TAG, "onPolylineClick: toString: " + polylineData.toString());
            if(polyline.getId().equals(polylineData.getPolyline().getId())){
                polylineData.getPolyline().setColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
                polylineData.getPolyline().setZIndex(1);
            }
            else{
                polylineData.getPolyline().setColor(ContextCompat.getColor(getActivity(), R.color.darkGrey));
                polylineData.getPolyline().setZIndex(0);
            }
        }
    }


    private class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        private LayoutInflater inflater;

        View markerView;
        ViewGroup windowContainer;
        TextView status;
        TextView restName;
        TextView updatedTime;
        TextView button;

        CustomInfoWindowAdapter(LayoutInflater inflater) {
            this.inflater = inflater;
        }

        // window frame for marker popup
        // return null, use default
        @Override
        public View getInfoWindow(Marker marker) {
            return null;
        }

        // window content for marker popup
        @Override
        public View getInfoContents(final Marker marker) {

            markerView = inflater.inflate(R.layout.custominfowindow, null, false);
            markerView.setLayoutParams(new ViewGroup.LayoutParams(350, 350));

            windowContainer = (ViewGroup) markerView.findViewById(R.id.window_container);
            restName = (TextView) markerView.findViewById(R.id.restName);

            if (restaurantMap.containsKey(marker.getId())) {
                Coordinate coordinate = restaurantMap.get(marker.getId()).coordinate;
                Restaurant restaurant = restaurantMap.get(marker.getId()).restaurant;

                restName.setText(String.format("%s Restaurant", restaurant.getName()));

                calculateDirections(marker);
                Log.e(TAG, "getInfoContents: "+ new LatLng(coordinate.getLatitude(), coordinate.getLongitude()) );
                return markerView;
            }
            // return default window
            return null;
        }
    }
    private void addPolylinesToMap(final DirectionsResult result){
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                //Log.d(TAG, "run: result routes: " + result.routes.length);

                if(mPolyLinesData.size()>0){
                    for(PolyLineData polyLineData: mPolyLinesData){
                        polyLineData.getPolyline().remove();

                    }
                    mPolyLinesData.clear();;
                    mPolyLinesData= new ArrayList<>();
                }
                for(DirectionsRoute route: result.routes){
                  //  Log.d(TAG, "run: leg: " + route.legs[0].toString());
                    List<com.google.maps.model.LatLng> decodedPath = PolylineEncoding.decode(route.overviewPolyline.getEncodedPath());

                    List<LatLng> newDecodedPath = new ArrayList<>();

                    // This loops through all the LatLng coordinates of ONE polyline.
                    for(com.google.maps.model.LatLng latLng: decodedPath){

//                        Log.d(TAG, "run: latlng: " + latLng.toString());

                        newDecodedPath.add(new LatLng(
                                latLng.lat,
                                latLng.lng
                        ));
                    }
                    Polyline polyline = googleMap.addPolyline(new PolylineOptions().addAll(newDecodedPath));
                    polyline.setColor(ContextCompat.getColor(getActivity(), R.color.darkGrey));
                    polyline.setClickable(true);
                    mPolyLinesData.add(new PolyLineData(polyline, route.legs[0]));


                }
            }
        });
    }
    /**
     * used to store restaurant and coordinate meta data for respective marker
     */
    private class RestaurantLocationMarkerMetadata implements ClusterItem {

        Restaurant restaurant;
        Coordinate coordinate;

        public RestaurantLocationMarkerMetadata(Restaurant restaurant, Coordinate coordinate) {
            this.restaurant = restaurant;
            this.coordinate = coordinate;
        }

        @Override
        public LatLng getPosition() {
            return new LatLng(coordinate.getLatitude(), coordinate.getLongitude());
        }



//        @Override
//        public String getTitle() {
//            // return null as we are to use a custom info window
//            return coordinate.getTitle();
//        }
//
//        @Override
//        public String getSnippet() {
//            // return null as we are to use a custom info window
//            return null;
//        }
    }

    private void setupClustering(List<RestaurantLocationMarkerMetadata> metadataList) {

        // add preconditions
        if ((metadataList == null || metadataList.isEmpty()) && googleMap == null) return;

        // Initialize the manager with the context and the map.
        ClusterManager<RestaurantLocationMarkerMetadata> clusterManager = new ClusterManager<>(getContext(), googleMap);
        clusterManager.addItems(metadataList);
        clusterManager.getClusterMarkerCollection().setOnInfoWindowClickListener(this);

        clusterManager.setRenderer(new DefaultClusterRenderer<RestaurantLocationMarkerMetadata>(getContext(),
                googleMap, clusterManager) {

            @Override
            protected void onBeforeClusterItemRendered(RestaurantLocationMarkerMetadata item, MarkerOptions markerOptions) {
                super.onBeforeClusterItemRendered(item, markerOptions);

                        BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED);

            }

            @Override
            protected void onClusterItemRendered(RestaurantLocationMarkerMetadata clusterItem, Marker marker) {
                super.onClusterItemRendered(clusterItem, marker);
                // maintain our marker list here
                if (restaurantMap == null) restaurantMap = new HashMap<>();
                restaurantMap.put(marker.getId(), clusterItem);
            }
        });


        clusterManager.getClusterMarkerCollection().setOnInfoWindowAdapter(
                new CustomInfoWindowAdapter(LayoutInflater.from(getContext())));
        // Point the map's listeners at the listeners implemented by the cluster
        // manager.
        googleMap.setOnCameraIdleListener(clusterManager);
    }

}
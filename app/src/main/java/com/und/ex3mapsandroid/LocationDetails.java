package com.und.ex3mapsandroid;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.und.ex3mapsandroid.base.BaseActivity;
import com.und.ex3mapsandroid.model.Coordinate;
import com.und.ex3mapsandroid.model.Restaurant;
import com.und.ex3mapsandroid.ui.ScrollCompatibleMapFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

import java.util.List;


public class LocationDetails extends BaseActivity implements GoogleMap.OnInfoWindowClickListener {

    private NestedScrollView container;
    private TextView restName, phone, email, openingHour, headOffice;
    private ImageView image;

    private double lat, lon;
    // using data binding here would be more fruitful
    private Coordinate coordinate;
    private Restaurant restaurant;
    private LatLng latLng;


    @Override
    public int layout() {
        return R.layout.rest_details;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar2);

        setSupportActionBar(toolbar);
//        toolbar.setNavigationIcon(R.drawable.common_google_signin_btn_icon_dark_normal);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        // Set Collapsing Toolbar layout to the screen
        CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);

        container = (NestedScrollView) findViewById(R.id.container);
        restName = (TextView) findViewById(R.id.rest_name);
        image = (ImageView) findViewById(R.id.image);


        restaurant = (Restaurant) getIntent().getSerializableExtra(Restaurant.EXTRA_KEY);


        restName.setText(restaurant.getName());


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        ScrollCompatibleMapFragment mapFragment = (ScrollCompatibleMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.setTouchListener(new ScrollCompatibleMapFragment.OnTouchListener() {
            @Override
            public void onTouch() {
                container.requestDisallowInterceptTouchEvent(true);
            }
        });

        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                lat = 47.923304;
                lon =  -97.086233;
                latLng = new LatLng(lat, lon);
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 11));
                setupClustering(restaurant.getCoordinateList(),googleMap);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    public void call(View view) {

        Intent callIntent = new Intent(Intent.ACTION_CALL);
//        Log.e("TAG", "call: " + restaurant.getPhone());
//        callIntent.setData(Uri.parse("tel:" + restaurant.getPhone()));
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) !=
                PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startActivity(callIntent);
    }

//    public void email(View view) {
////        String to = restaurant.getEmail();
//        Intent email = new Intent(Intent.ACTION_SEND);
//        email.putExtra(Intent.EXTRA_EMAIL, new String[]{to});
//        email.setType("message/rfc822");
//        startActivity(Intent.createChooser(email, "Choose an Email client :"));
//    }

    private void setupClustering(List<Coordinate> metadataList, GoogleMap googleMap) {

        // add preconditions
        if ((metadataList == null || metadataList.isEmpty()) && googleMap == null) return;

        // Initialize the manager with the context and the map.
        ClusterManager<Coordinate> clusterManager = new ClusterManager<>(this, googleMap);
        clusterManager.addItems(metadataList);
        clusterManager.getClusterMarkerCollection().setOnInfoWindowClickListener(this);
        clusterManager.setRenderer(new DefaultClusterRenderer<Coordinate>(this,
                googleMap, clusterManager) {

            @Override
            protected void onBeforeClusterItemRendered(Coordinate item, MarkerOptions markerOptions) {
                super.onBeforeClusterItemRendered(item, markerOptions);
//                // add in code to change marker bitmap color, something like this
//                try {
//                    markerOptions.icon(item.getStatus().equalsIgnoreCase("true") ?
//                            BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE) :
//                            BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)
//                    );
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
            }

            @Override
            protected void onClusterItemRendered(Coordinate clusterItem, Marker marker) {
                super.onClusterItemRendered(clusterItem, marker);

            }
        });

        googleMap.setOnCameraIdleListener(clusterManager);
    }


    @Override
    public void onInfoWindowClick(Marker marker) {

    }
}


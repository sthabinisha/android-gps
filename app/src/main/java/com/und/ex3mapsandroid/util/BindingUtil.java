package com.und.ex3mapsandroid.util;

import android.databinding.BindingAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;

import com.und.ex3mapsandroid.RecyclerAdapter;
import com.und.ex3mapsandroid.model.Restaurant;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Helper class for our data binding
 */
public class BindingUtil {

    @BindingAdapter({"rest_list", "list_adapter"})
    public static void initializeRecyclerView(RecyclerView view, List<Restaurant> restaurantList, RecyclerAdapter adapter) {
        adapter.setRestaurantList(restaurantList);
        view.setLayoutManager(new LinearLayoutManager(view.getContext()));
        view.setAdapter(adapter);
    }

    @BindingAdapter("image_url")
    public static void loadImage(ImageView view, String url) {
        Picasso.with(view.getContext()).load(url).into(view);
    }
}

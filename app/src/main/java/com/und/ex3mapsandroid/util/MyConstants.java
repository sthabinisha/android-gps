package com.und.ex3mapsandroid.util;



public class MyConstants {

    public static final String KEY_ADDRESS = "address";
    public static final String KEY_REST_LIST = "location";
    public static final String KEY_LOCATION = "location";

    public static final String KEY_NAME = "name";

}

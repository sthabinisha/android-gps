package com.und.ex3mapsandroid.model;


import java.io.Serializable;
import java.util.List;


public class Restaurant implements Serializable {

    public static final String EXTRA_KEY = "restaurant";

    private String name;
    private String address;

    private List<Coordinate> coordinateList;

    public Restaurant() {
    }

    public Restaurant(String name, String address, String email, String headOffice, String openingHours,
                      String phone, String url, List<Coordinate> coordinateList) {
        this.name = name;
        this.address = address;

        this.coordinateList = coordinateList;
    }



    public List<Coordinate> getCoordinateList() {
        return coordinateList;
    }

    public void setCoordinateList(List<Coordinate> coordinateList) {
        this.coordinateList = coordinateList;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
//


    @Override
    public String toString() {
        return "Restaurant{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +

                ", coordinateList=" + coordinateList +
                '}';
    }

}
